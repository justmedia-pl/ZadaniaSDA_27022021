import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wagę:");
        float userWeight = scanner.nextFloat();
        System.out.println("Podaj wzrost:");
        int userHeight = scanner.nextInt();

        float bmi = (float) (userWeight/Math.sqrt(userHeight));
        if (bmi >= 18.5 && bmi <= 24.9) System.out.println("BMI Optymalne (Twoje bmi:"+bmi+")");
        else System.out.println("BMI nieoptymalne (Twoje bmi:"+bmi+")");
    }
}
