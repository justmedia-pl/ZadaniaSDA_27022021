import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        float userIn;
        float pi = 3.14f;
        System.out.println("podaj promień:");
        Scanner scanner = new Scanner(System.in);
        userIn = scanner.nextFloat();
        System.out.println("With def PI - Obówd: "+2*pi*userIn+", Pole:"+pi*Math.sqrt((double) userIn)+"^2");
        System.out.println("With MATH PI - Obówd:"+2*Math.PI*userIn+", Pole:"+Math.PI*Math.sqrt((double) userIn)+"^2");
    }
}
