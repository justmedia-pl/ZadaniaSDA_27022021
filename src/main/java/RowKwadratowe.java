import java.util.Scanner;

public class RowKwadratowe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj a:");
        int a = scanner.nextInt();
        System.out.println("Podaj b:");
        int b = scanner.nextInt();
        System.out.println("Podaj c:");
        int c = scanner.nextInt();

        double delta = Math.sqrt(b) - (4*a*c);
        if (delta < 0 ) System.out.println("Delta mniejsza od zera!");
       else if (delta == 0){
            double x = -b/2*a;
            System.out.println("Równanie ma jedno rozmwiąznie x="+x);
        } else {
            double x1 = -b - delta / 2*a;
            double x2 = -b + delta / 2*a;
            System.out.println("Równanie ma dwa rozwiązania  x1="+x1+", x2="+x2);
        }
    }
}
