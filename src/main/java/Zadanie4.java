import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dodatnią liczbę całkowią:");
        int a = scanner.nextInt();
        if (a < 0 ) System.out.println("Podana liczba jest liczbą ujemną!");
        else{
            for (int i=1;i<=a;i++){
                if (i%3 == 0 && i%7 ==0) System.out.println("Pif Paf");
                else if (i%7 == 0 ) System.out.println("Paf");
                else if (i%3 == 0 ) System.out.println("Pif");
                else System.out.println(i);
            }
        }
    }
}
