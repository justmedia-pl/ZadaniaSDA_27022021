import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę a:");
        float a = scanner.nextFloat();
        System.out.println("Podaj działanie(+-/*):");
        String operator = scanner.next();
        if (!operator.matches("^[+-/*]{1}$")) System.out.println("Błędny znak");
        else {
            System.out.println("Podaj liczbę b:");
            float b = scanner.nextFloat();
            switch (operator){
                case ("+"):
                    System.out.println(a+b);
                    break;
                case ("-"):
                    System.out.println(a-b);
                    break;
                case ("*"):
                    System.out.println(a*b);
                    break;
                case ("/"):
                    System.out.println(a/b);
                    break;
            }
        }


    }
}
