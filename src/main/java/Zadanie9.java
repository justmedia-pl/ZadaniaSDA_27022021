import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int height = 4;
        System.out.println("Podaj liczbę iteracji:");
        int a = scanner.nextInt();
        for(int i=0;i<=height;i++){
            System.out.println(i);
            for (int j=0;j<a+1;j++){

                if (i == j || 2*i+2*height == j ) System.out.print("*");
                else if ((i <= height && 2*height+1-i == j))System.out.print("*");

                else System.out.print("_");
            }
            System.out.print("\n");
        }

    }
}
